#include "function.h"

int main(int argc, char *argv[])
{
    /* 
        int argc -> le nombres d'argument 
        et argv  -> le tableau des arguments 
    */ 
/*    if (argc < 4 || argc > 4){
        printf("Error ! \n");
        printf("Vous avez les arguments suivants : \n");
        for (int i = 0; i < argc; i++){
            printf("%s \n", argv[i]);       
        }
    }
    else if (argc == 4){
        printf("Welcome ! \n");
    }
*/
    phrase ph1= "Voici une phrase !";
    phrase ph2 = malloc(strlen(ph1) * sizeof(char) + 1);
    affiche(ph1);
    majuscules(ph1, ph2);

    //miroir(ph1, ph2);
    printf("%s \n", ph2);
    free(ph2);

return 0;
}