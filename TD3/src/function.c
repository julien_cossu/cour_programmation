#include "function.h"

void affiche(phrase p){
    for (int i = 0; p[i] != '\0'; i++){
        if(p[i]!=' '){
            printf("%c", p[i]);
        }
        else printf("\n");
    }
    printf("\n");
}

void miroir(phrase s, phrase cible){
    int j = strlen(s) -1;
    
    for (int i=j;i>=0;i--){
        //printf("%c", s[i]);
        cible[j-i]=s[i];
    }
}

void majuscules(phrase s, phrase cible){
    int j = strlen(s) -1;
    
    for (int i=0;i<j;i++){
        if((s[i] >= 'a') && (s[i] <= 'z')){
            int new = (s[i] - 'a') + 'A';
            cible[i]=new;
        } else {
            cible[i]=s[i];
        }
        //printf("%d\n", s[i]);
    }
}
