#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef char* phrase;
void affiche(phrase p);
void miroir(phrase s, phrase cible);
void majuscules(phrase s, phrase cible);
