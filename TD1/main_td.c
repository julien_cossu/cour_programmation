#include <stdio.h>
#include "exo.h"

int main(void)
{
    int exo=1;

    printf("Voici les exercies de TD :");
    scanf("%d", &exo);

    switch (exo)
    {
    case 1:
        td1_exo1_hello();
        break;
    case 2:
        td1_exo2();
        break;
    case 3:
        td1_exo3_ASCII();
        break;
    case 4:
        td1_exo4_MaxMin();
        break;
    case 5:
        td1_exo5();
        break;
    /*case 6:
        td1_exo6_calculatrice();
        break;*/
    case 7:
        td1_exo7_tableau();
        break;
    case 8:
        td1_exo8_matrice();
        break;
    case 9:
        td2_exo2_tab();
        break;
    default:
        break;
    }

    return 0;
}