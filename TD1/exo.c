#include <stdio.h>
#include <stdlib.h>

void td1_exo1_hello(void){
    printf("Hello new World ! \n");
}

void td1_exo2(void){
    int nb1, nb2 = 0;

    printf("Entrer le premier nombre :");
    scanf("%d", &nb1);
    printf("Entrer le deuxième nombre :");
    scanf("%d", &nb2);

    int res = nb1*nb2;

    printf("Le produit est : %d \n\n", res);
}

void td1_exo3_ASCII(void){
    char value;
    //printf("Entrer :");
    scanf("Entrer : %c", &value);

    printf("%c = %d\n", value, value);
}

void td1_exo4_MaxMin(void){
    int value;
    printf("Entrer :");
    scanf("%d", &value);
    int petit, grand = value;
    while (value != 0) {
        printf("Entrer :");
        scanf("%d", &value);
        if(value < petit && value != 0) {
            petit = value;
        }
        else if (value > petit) {
            if(value > grand) {
                grand = value;
            }
        }
    }
    printf(" Le plus grand est : %d", grand);
    printf(" Le plus petit est : %d", petit);
}

void td1_exo5(void){
    int nb=0;

    printf("Entrer un nombre :");
    scanf("%d", &nb); 
    //printf("%d", nb);

    for (int i = 1 ; i < nb ; i++)
    {
        if ((nb%i)==0) printf("%d\n",i);
    }
}
/*
void td1_exo6_calculatrice(void){
    int iOperande1, iOperande2;
    char cSigne;
    int iReslutOK=1;

    while (iReslutOK == 1){
        printf("-> opérende");
        scanf("%d %c %d", &iOperande1, &cSigne, &iOperande2);
    }
}
*/
void parcourTab(int tab[]){
    for (int i = 0 ; i < 5 ; i++)
    {
        printf("%d\n", tab[i]);
    }
}

void td1_exo7_tableau(void){
    int tab1[5]={0,1,2,3,0};

    parcourTab(&tab1);
}

void td1_exo8_matrice(void){
    const int NB_COL = 5, NB_LI = 5; 

    int iMatrice[5][5] =
    {
        {1,   2,  3,  4,  5},
        {11, 12, 13, 14, 15},
        {21, 22, 23, 24, 25},
        {31, 32, 33, 34, 35},
        {41, 42, 43, 44, 45}
    };

    for (int iLigne = 0; iLigne < NB_LI; iLigne++)
    {
        for (int iColonne = 0; iColonne < NB_COL; iColonne++)
        {
            printf("%d \n", iMatrice[iLigne][iColonne]);
        }
    }
}

void td2_exo2_tab(void){
    int tab1[5]={0,1,2,3,0};

    for (int i = 0 ; i < 5 ; i++)
    {
        printf("%d\n", *(tab1+i));
    }
}
